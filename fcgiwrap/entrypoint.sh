#!/bin/bash

set -e

SOCK=/fcgiwrap/run/fcgi.sock

rm -f $SOCK

/usr/sbin/fcgiwrap -c 2 -s unix:$SOCK
