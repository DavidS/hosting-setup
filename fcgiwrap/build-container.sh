#!/bin/bash

set -ex

container="$(buildah from debian:buster-slim)"
buildah run "$container" apt-get update
buildah run "$container" apt-get install -y fcgiwrap
buildah run "$container" sh -c 'rm -Rf /var/cache/apt/*'
buildah config --created-by "David Schmitt <david@black.co.at>" "$container"
buildah run "$container" mkdir -p /fcgiwrap/run /fcgiwrap/cgi-bin
buildah config --volume /fcgiwrap/run "$container"
buildah config --volume /fcgiwrap/cgi-bin "$container"

buildah copy "$container" entrypoint.sh /usr/local/bin/
buildah config --cmd '' "$container"
buildah config --workingdir '/fcgiwrap/cgi-bin' "$container"
buildah config --entrypoint '["/usr/sbin/fcgiwrap", "-c", "2", "-s", "unix:/fcgiwrap/run/fcgi.sock"]' "$container"

buildah commit --rm "$container" fcgiwrap
